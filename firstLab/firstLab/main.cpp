//
//  main.cpp
//  firstLab
//
//  Created by Ilya Onishenko on 18.09.15.
//  Copyright (c) 2015 Ilya Onishenko. All rights reserved.
//

#include <iostream>
#include <time.h>
using namespace std;

long double printBinary(double fraDecimal){
    long double fraBinary,bFractional = 0.0,dFractional,fraFactor=0.1;
    long int dIntegral,bIntegral=0;
    long int intFactor=1,remainder,temp,i;
    dIntegral = fraDecimal;
    dFractional =  fraDecimal - dIntegral;
    
    while(dIntegral!=0){
        remainder=dIntegral%2;
        bIntegral=bIntegral+remainder*intFactor;
        dIntegral=dIntegral/2;
        intFactor=intFactor*10;
    }
    
    for(i=1;i<=6;i++){
        
        dFractional = dFractional * 2;
        temp =  dFractional;
        
        bFractional = bFractional + fraFactor* temp;
        if(temp ==1)
            dFractional = dFractional - temp;
        
        fraFactor=fraFactor/10;
    }
    
    fraBinary =  bIntegral +  bFractional;
//    cout<<"\n"<<fraBinary<<"\n";
    return fraBinary;
}


int main(int argc, const char * argv[]) {
    // insert code here...
    cout << "Hello, World!\n";
    int t=0,t1=0,t2=0,t3=0;
    double a1=0,a2=0,a3=0;
    bool b1=0,b2=0,b3=0;
    while(t!=4){
        cout<<"Выберите пункт меню:\n1)Операции с арифметическими данными\n2)Операции с логическими данными\n3)Операции с текстовыми данными\n4)Выход\n";
        cout<<"Введите пункт\n";
        cin>>t;
        switch(t){
            case 1:{
                t1=0;
                cout<<"Вы выбрали операции с арифметическими данными\n";
                cout<<"Выберите интересующую вас арифметическую операцию\n";
                cout<<"1)Сложение\n2)Вычитание\n3)Умножение\n4)Деление\n";
                cout<<"Выберите пункт\n";
                cin>>t1;
                switch (t1) {
                            //TODO проверить на отрицательных числах
                        case 1:{
                            a1=0,a2=0,a3=0;
                            cout<<"Сложение\n";
                            cout<<"Введите а1\n";
                            cin>>a1;
//                            scanf("%lf",&a1);
                            cout<<"Представление в двоичной форме: "<<printBinary(a1)<<"\n";
                            cout<<"Введите a2\n";
                            cin>>a2;
                            cout<<"Представление в двоичной форме: "<<printBinary(a2)<<"\n";
                            a3 = a1+a2;
                            cout<<"Представление в двоичной форме ответа: "<<printBinary(a3)<<"\n";
                            break;
                        }
                        case 2:{
                            a1=0,a2=0,a3=0;
                            cout<<"Вычитание\n";
                            cout<<"Введите а1\n";
                            cin>>a1;
                            cout<<"Представление в двоичной форме: "<<printBinary(a1)<<"\n";
                            cout<<"Введите a2\n";
                            cin>>a2;
                            cout<<"Представление в двоичной форме: "<<printBinary(a2)<<"\n";
                            a3 = a1-a2;
                            cout<<"Представление в двоичной форме ответа: "<<printBinary(a3)<<"\n";
                            break;
                        }
                        case 3:{
                            a1=0,a2=0,a3=0;
                            cout<<"Умножение\n";
                            cout<<"Введите а1\n";
                            cin>>a1;
                            cout<<"Представление в двоичной форме: "<<printBinary(a1)<<"\n";
                            cout<<"Введите a2\n";
                            cin>>a2;
                            cout<<"Представление в двоичной форме: "<<printBinary(a2)<<"\n";
                            a3 = a1*a2;
                            cout<<"Представление в двоичной форме ответа: "<<printBinary(a3)<<"\n";
                            break;
                        }
                        case 4:{
                            a1=0,a2=0,a3=0;
                            cout<<"Деление\n";
                            cout<<"Введите а1\n";
                            cin>>a1;
                            cout<<"Представление в двоичной форме: "<<printBinary(a1)<<"\n";
                            cout<<"Введите a2\n";
                            cin>>a2;
                            cout<<"Представление в двоичной форме: "<<printBinary(a2)<<"\n";
                            a3 = a1/a2;
                            cout<<"Представление в двоичной форме ответа: "<<printBinary(a3)<<"\n";
                            break;
                        }
                    }
                break;
            }
            case 2:{
                t2=0;
                cout<<"Вы выбрали операции с логическими данными\n";
                cout<<"Выберите интересующую вас арифметическую операцию\n";
                cout<<"1)ИЛИ\n2)И\n3)НЕ\n";
                cout<<"Выберите пункт\n";
                cin>>t2;
                switch (t2) {
                        case 1:{
                            cout<<"Вы выбрали ИЛИ\n";
                            b1=0,b2=0,b3=0;
                            cout<<"Введите b1 (0 или 1)\n";
                            cin>>b1;
                            cout<<"b1 = "<<b1<<endl;
                            cout<<"Введите b2 (0 или 1)\n";
                            cin>>b2;
                            cout<<"b2 = "<<b2<<endl;
                            cout<<"b3 = b1||b2\n";
                            b3 = b1||b2;
                            cout<<"b3 = "<<b3<<endl;
                            break;
                        }
                        case 2:{
                            cout<<"Вы выбрали И\n";
                            b1=0,b2=0,b3=0;
                            cout<<"Введите b1 (0 или 1)\n";
                            cin>>b1;
                            cout<<"b1 = "<<b1<<endl;
                            cout<<"Введите b2 (0 или 1)\n";
                            cin>>b2;
                            cout<<"b2 = "<<b2<<endl;
                            cout<<"b3 = b1&&b2\n";
                            b3 = b1&&b2;
                            cout<<"b3 = "<<b3<<endl;
                            break;
                        }
                        case 3:{
                            cout<<"Вы выбрали НЕ\n";
                            b1=0,b2=0,b3=0;
                            cout<<"Введите b1 (0 или 1)\n";
                            cin>>b1;
                            cout<<"b1 = "<<b1<<endl;
                            cout<<"Введите b2 (0 или 1)\n";
                            cin>>b2;
                            cout<<"b2 = "<<b2<<endl;
                            cout<<"b3 = b1!=b2\n";
                            b3 = b1!=b2;
                            cout<<"b3 = "<<b3<<endl;
                            break;
                        }
                            
                        default:{
                            cout<<"Вы нажали куда-то не туда !";
                            break;
                        }
                    }
                break;
            }
            case 3:{
                cout<<"Вы выбрали операции с текстовыми данными\n";
                t3=0;
                cout<<"Ввести слово и вывести в hex\n";
                string line;
                        cin>>line;
                        char ch[line.length()-1];
                        for(int i=0;i<line.length();i++){
                            ch[i]=line[i];
                        }
                        for(int i=0;i<line.length();i++){
                            cout<<hex<<(int)ch[i];
                        }
                cout<<endl;
                break;
            }
            case 4:{
                cout<<"Выход\n";
                break;
            }
            default:{
                cout<<"Вы нажали что-то не то\n";
                break;
            }
        }
    }
    return 0;
}
